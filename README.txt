
Why Gmap Block?

Purpose of this Module:
This module has been created with having few things in mind:
- Not to burden the Drupal engine to process lots of code just to show simple google map on any of the page.
- No dependency must be there for such a small feature.
- Must be true block, which is configurable from backend. You can configure its height,width,labels,controls,pins all from backend.

Upcoming Features:
- Must be working with "Multi-block" module, in case if more than one maps is to be shown on single page.

Installation
------------
Copy gmapblock.module to your module directory and then enable it from Admin.
Go to Structure->Blocks & find "Google Map Block" & Click on "configure" next to it & fill in all the information as you can see in screenshot.

Note:
- Make sure you are using Full Html format for textarea so div's can be used.
- Make sure you add "<div class='google-map-outer'><div id='map-loc'>Loading...</div></div>" to body field of the block for map to appear on front end.
- Make sure to add gmap API key.
- Make sure to put in valid address or latitude or longitude.


Author
------
Arun Verma
av@fabwebstudio.com
